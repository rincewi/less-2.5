#!/bin/bash

ID_BUILD=$(curl -k --location --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_BUILD/pipelines?per_page=1&page=1" | jq '.[] | .id')
JOB_ID=$(curl --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_BUILD/pipelines/$ID_BUILD/jobs" | jq '.[] | select(.artifacts_file) | .id')

curl --output log.txt --location --header "PRIVATE-TOKEN: $MY_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_BUILD/jobs/$JOB_ID/artifacts/log.txt"
